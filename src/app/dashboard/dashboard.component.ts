import { Component } from '@angular/core';
import { ProfileService } from '../profile/services/profile.service';
import { DiagnosticsService, ConfigurationService } from '@universis/common';
import { CurrentRegistrationService } from '../registrations/services/currentRegistrationService.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  providers: [ProfileService]
})
export class DashboardComponent {
  public student: any;
  public diningServiceEnabled: boolean = false;
  /*
     if is true it will shown component progress-bar-semester
     if is false it will shown component progress-bar-degree
    */
  public viewsemester = false;

  constructor(private _profileService: ProfileService,
    private _diagnostics: DiagnosticsService,
    private _configurationService: ConfigurationService,
    private _currentRegistrationService: CurrentRegistrationService) {
    this._profileService.getStudent().then(res => {
      this.student = res; // Load data

      // If student is active, enableAvailableClassesPreFetch setting is true and availableClasses isn't saved in sessionStorage
      // get availableClasses after 60 to 180 seconds
      if (
        this.student &&
        this.student.studentStatus.alternateName === "active" &&
        this._configurationService.settings.app &&
        this._configurationService.settings.app['enableAvailableClassesPreFetch'] === true &&
        !sessionStorage['availableClasses']
      ) {
        // Check if the registrationPeriodStart is in less than the prefetchMaximumTimeDifference setting from config file or 2 hours if it is not set
        const timeDifference: number = this.student.department?.registrationPeriodStart - Date.now();
        const maxTimeDifference: number = !this._configurationService?.settings?.app['prefetchMaximumTimeDifference'] ? 2 : this._configurationService?.settings?.app['prefetchMaximumTimeDifference'];
        if (timeDifference < 0 || (timeDifference / (1000 * 3600)) > maxTimeDifference) {
          return;
        }

        setTimeout((): void => {
          // Check again if availableClasses in sessionStorage has been updated during the setTimeout timer delay or the prefetch has already started
          if (!sessionStorage['availableClasses'] && sessionStorage['availableClassesPrefetching'] !== 'true') {
            sessionStorage.setItem('availableClassesPrefetching', 'true');
            this._currentRegistrationService.getAvailableClasses().then(() => sessionStorage.removeItem('availableClassesPrefetching'));
          }
        }, Math.floor(Math.random() * (180 - 60) + 60) * 1000);
      }
    });
    // check if dining service is enabled
    // do not await this call
    this._diagnostics.hasService('DiningService').then((hasDiningService: boolean) => {
      this.diningServiceEnabled = hasDiningService;
    }).catch(err => {
      // just log the error
      console.error(err);
    })
  }

}
