import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, LoadingService} from '@universis/common';
import {ErrorService} from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-registrations-list',
  templateUrl: './registrations-list.component.html',
  styleUrls: ['./registrations-list.component.scss']
})
export class RegistrationListComponent implements OnInit {

  public loading = true;
  public registrations: any = [];
  public currentLanguage;
  public allRegistrations: any;
  private period: any;
  public periods: any = [];
  private chosenPeriod: any;
  public defaultLanguage: string | any = "";
  public showTeachingHours: boolean = true;

  constructor(private context: AngularDataContext,
              private _configurationService: ConfigurationService,
              private loadingService: LoadingService,
              private _errorService: ErrorService,
              private profileService: ProfileService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService?.settings?.i18n?.defaultLocale;
  }

async ngOnInit() {
  try {
    this.loadingService.showLoading();

    // get student
    const student = await this.profileService.getStudent();

    // get showTeachingHours flag
    this.showTeachingHours = student?.department?.organization?.instituteConfiguration?.showTeachingHours ?? false;

    // Fetch registrations using await and try/catch block
    try {
      const res = await this.context.model('students/me/registrations')
        .asQueryable()
        .expand('classes($orderby=semester,course/displayCode;$expand=course($expand=locale),' +
          'courseClass($expand=instructors($expand=instructor($select=InstructorSummary))),courseType($expand=locale))')
        .orderBy('registrationYear desc')
        .thenBy('registrationPeriod desc')
        .getItems();

      this.registrations = this.allRegistrations = res;
      this.initializePeriods();
      this.loading = false;
    } catch (err) {
        throw err;
    } finally {
        this.loadingService.hideLoading();
    }
  } catch (err) {
    // Handle the error that may occur in this.profileService.getStudent()
    console.error("Error fetching student:", err);
  } finally {
    this.loadingService.hideLoading();
  }
}

  // Initialize periods for the left menu in order to avoid double period names in the list
  initializePeriods() {
    this.registrations.forEach(registration => {
      // if period is not already in the array, push it
      if (!this.periods.includes(registration.registrationYear.alternateName)) {
        this.periods.push(registration.registrationYear.alternateName);
      }
    });
  }

  filterByPeriod(period) {
    this.registrations = this.allRegistrations.filter(x => {
      return x.registrationYear.alternateName === period;
    });
    this.chosenPeriod = period;
  }

  removeSemesterFiltering() {
    this.registrations = this.allRegistrations;
    this.chosenPeriod = '';
  }
}
