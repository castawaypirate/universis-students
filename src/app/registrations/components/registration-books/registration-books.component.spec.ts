import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationBooksComponent } from './registration-books.component';

describe('RegistrationBooksComponent', () => {
  let component: RegistrationBooksComponent;
  let fixture: ComponentFixture<RegistrationBooksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationBooksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrationBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
