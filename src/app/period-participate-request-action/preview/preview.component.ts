import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ErrorService, ModalService, LoadingService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ResponseError } from '@themost/client';


@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html'
})
export class PreviewComponent implements OnInit, OnDestroy {

  public subscription?: Subscription;
  public data: any;
  public department: any;
  public loading = true;
  constructor(private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _loadingService: LoadingService,
    private _activatedRoute: ActivatedRoute) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.showLoading(true);
    this.subscription = this._activatedRoute.params.subscribe(params => {
        this._context.model('PeriodParticipateRequestActions').where('id').equal(params['id'])
          .expand('year,period')
          .getItem().then( result => {
            if (result == null) {
              throw new ResponseError('Not Found', 404);
            }
            // set reject for ui
            result.reject = !result.agree;
            this.data = result;
            this.showLoading(false);
          }).catch( err => {
            this.showLoading(false);
            console.error(err);
            this._errorService.showError(err);
          });
    }, err => {
      this.showLoading(false);
      console.error(err);
      this._errorService.showError(err);
    });
  }

}
