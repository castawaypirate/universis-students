import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { TeachingEventComponent } from './components/teaching-event/teaching-event.component';

const routes: Routes = [
  {
    path: '',
    component: TeachingEventComponent,
    data: {
      title: 'Teaching Events Schedule'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeachingEventRoutingModule { }
