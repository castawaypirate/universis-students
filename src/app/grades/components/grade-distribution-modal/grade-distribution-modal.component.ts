import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService, LoadingService, ModalService } from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChartOptions, ChartDataset, ChartConfiguration } from 'chart.js';
import { CourseGradeDistributionBase } from '../../services/grades.service';
import { TranslateService } from '@ngx-translate/core';

import * as el from "../../i18n/grades.el.json"
import * as en from "../../i18n/grades.en.json"



@Component({
  selector: 'app-grades-distribution-modal',
  templateUrl: './grade-distribution-modal.component.html'
})
export class GradeDistributionModalComponent implements OnInit, OnDestroy {
  @Input() modalCourse?: CourseGradeDistributionBase;
  @Input() modalRef?: BsModalRef | any;
  public isLoading: boolean = false;
  public gradeScaleIsNumeric: boolean = false;
  public studentsRegistered: any;
  public studentsGraded?: number | any;
  public studentsSucceeded?: number | any;
  public avgGradeStudentsSucceeded?: number;
  public avgGradeStudentsGraded?: number;
  public gradeScaleValues: Array<any> = [];
  public legends: Array<string> = [];

  public barChartOptions: ChartConfiguration<'bar'>['options'] = {};

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [], datasets: [{ data: [] }]
  };
  public readonly barChartType = 'bar';
  public barOptions: ChartOptions = {
    scales: {
      x: {},
      y: {}
    },
    responsive: true
  };


  public chartColors: Array<any> = [
    { backgroundColor: ['#c95485', '#c4457a', '#ba3b70', '#ab3666', '#9b315d', '#005d8f', '#006aa3', '#0077b8', '#0085cc', '#0092e0', '#009ff5'] },
  ]


  constructor(private gradesService: GradesService,
    private gradesScale: GradeScaleService,
    private modalService: ModalService,
    private loadingService: LoadingService,
    private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
  ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);

    this.barChartOptions = {
      responsive: false,
      scales: { 
        x: { ticks: { precision: 0 }, title: { display: true, text: this._translateService.instant('Grades.Chart.Grade') } }, 
        y: { ticks: { display: true, stepSize: 1 }, title: { display: true, text: this._translateService.instant('Grades.Chart.Students') } } 
      }
    };

    this.isLoading = true;
    this.loadingService.showLoading();
    this.gradesScale.getGradeScale(this.modalCourse?.gradeScaleId).then((gradeScale: | any) => {
      this.gradesService.getGradesStatistics(this.modalCourse?.examId).then((result) => {
        const data = result;

        this.studentsRegistered = data
          // map count
          .map(x => x.total)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students with grade
        this.studentsGraded = data
          // filter students by grade
          .filter(x => {
            return x.examGrade != null;
          })
          // map count
          .map(x => x.total)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students passed
        this.studentsSucceeded = data
          // filter passed grades
          .filter(x => {
            return x.isPassed;
          })
          // map count
          .map(x => x.total)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        if(this.studentsSucceeded==0){this.avgGradeStudentsSucceeded=0}
        else{
        // get average grade of succeeded students
        this.avgGradeStudentsSucceeded = data
          // filter passed grades
          .filter(val => val.isPassed)
          // map passed grades * total grades 
          .map(val => Number((val.examGrade / gradeScale.scaleFactor).toFixed(7)) * val.total)
          // calcuate average Grade of succeeded students
          .reduce((acc, val) => acc + val, 0) / this.studentsSucceeded;}
        // keep one decimal point  
        this.avgGradeStudentsSucceeded = Number(this.avgGradeStudentsSucceeded.toFixed(1))

        if(this.studentsGraded==0){this.avgGradeStudentsGraded=0}
        else{
        //get average grade of graded students
        this.avgGradeStudentsGraded = data
          // map  grades * total grades 
          .map(val => Number((val.examGrade / gradeScale.scaleFactor).toFixed(7)) * val.total)
          // calcuate average grade of Graded students
          .reduce((acc, val) => acc + val, 0) / this.studentsGraded;}
        // keep one decimal point  
        this.avgGradeStudentsGraded = Number(this.avgGradeStudentsGraded.toFixed(1))




        // get grade scale values
        this.gradeScaleValues = [];
        // numeric grade scale
        if (gradeScale.scaleType === 0) {
          this.gradeScaleIsNumeric = true;
          // get grade scale base
          const gradeScaleMinValue = parseInt(gradeScale.format(0), 10);
          // get grade scale max
          const gradeScaleMaxValue = parseInt(gradeScale.format(1), 10);

          for (let grade = gradeScaleMinValue; grade < gradeScaleMaxValue + 1; grade++) {
            if (grade === gradeScaleMaxValue) {
              this.gradeScaleValues.push({
                valueFrom: gradeScale.convert(grade),
                name: `${grade}`,
                total: 0
              });
              continue;
            }
            this.gradeScaleValues.push({
              valueFrom: gradeScale.convert(grade),
              valueTo: gradeScale.convert(grade + 1),
              name: `${grade}`,
              total: 0
            });
          }
        } else {
          this.gradeScaleValues = gradeScale['values'].map(value => {
            return {
              valueFrom: value.valueFrom,
              valueTo: value.valueTo,
              name: value.name,
              total: 0
            };
          });
        }

        if (gradeScale.scaleType === 1) {
          this.gradeScaleValues.reverse();
        }

        // get total students per value
        this.gradeScaleValues.forEach(value => {
          value.total = data.filter(x => {
            if (value.valueTo) {
              return x.examGrade >= value.valueFrom && x.examGrade < value.valueTo;
            }
            else {
              return x.examGrade >= value.valueFrom;
            }
          })
            .map(x => x.total)
            .reduce((a, b) => a + b, 0);
        });

        // get chart data
        const _chartGradesData: any = this.gradeScaleValues.map(x => x.total);
        this.legends = this.gradeScaleValues.map(x => x.name);

        this.barChartData = {
          labels: this.legends,
          datasets: [
            { data: [...(_chartGradesData)], backgroundColor: ['#c95485', '#c4457a', '#ba3b70', '#ab3666', '#9b315d', '#005d8f', '#006aa3', '#0077b8', '#0085cc', '#0092e0', '#009ff5'] }
          ]
        };
        this.isLoading = false;
        this.loadingService.hideLoading();
      }).catch(err => {
        this.isLoading = false;
        console.log(err)
        this.loadingService.hideLoading();
        throw err;
      });
    }).catch(err => {
      console.log(err)
      this.isLoading = false;
      this.loadingService.hideLoading();
      throw err;
    });

  }

  closeModal() {
    this.modalCourse = {
      name: '',
      examPeriodName: '',
      examPeriodYear: '',
      examId: 0,
      displayCode: '',
      gradeScaleId: 0
    };
    this.modalRef.hide();
    this.ngOnDestroy();
  }

  ngOnDestroy() {
    this.avgGradeStudentsGraded = 0;
    this.avgGradeStudentsSucceeded = 0;
    this.studentsSucceeded = 0;
    this.studentsGraded = 0;
    this.studentsRegistered = 0;
  }
}
