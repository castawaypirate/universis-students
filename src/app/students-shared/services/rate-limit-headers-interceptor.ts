import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@universis/common';
import { Observable, tap } from 'rxjs';
import * as moment from 'moment';

@Injectable()
export class RateLimitHeadersInterceptor implements HttpInterceptor {

  constructor(private toast: ToastService, private translate: TranslateService) {
    window.addEventListener('beforeunload', () => {
      sessionStorage.removeItem('slowDownLimit');
    }, {
      once: true
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
        if (event.type === HttpEventType.Response) {
          const headers = event.headers;
          const slowDownLimit = parseInt(headers.get('X-SlowDown-Limit') || '0', 10);
          if (slowDownLimit > 0) {
            const slowDownRemaining = parseInt(headers.get('X-SlowDown-Remaining') || '0', 10);
            if (slowDownRemaining === 0) {
              // if the slow down limit is on, do not show the message
              const onSlowDownLimit = sessionStorage.getItem('slowDownLimit');
              if (onSlowDownLimit === 'on') {
                return;
              }
              // set the slow down limit on
              sessionStorage.setItem('slowDownLimit', 'on');
              // get the reset time
              const slowDownReset = parseInt(headers.get('X-SlowDown-Reset') || '0', 10);
              // show message for informing the user
              if (slowDownReset > 0) {
                const momentWithLocale = moment(slowDownReset * 1000).locale(this.translate.currentLang);
                const resetIn = momentWithLocale.fromNow(true);
                this.toast.show(this.translate.instant('SlowDownMessage.Title') , this.translate.instant('SlowDownMessage.Message', {
                  resetIn
                }));
              } else {
                this.toast.show(this.translate.instant('SlowDownMessage.Title') , this.translate.instant('SlowDownMessage.MessageWithoutReset'));
              }
            } else {
              // if slow down limit is not on, remove the session storage because the limit is reset
              sessionStorage.removeItem('slowDownLimit');
            }
          }
        }
      }));
  }
  
  
}
