import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MostModule} from '@themost/angular';
import {SharedModule} from '@universis/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AdvancedFormsModule} from '@universis/forms';
import {environment} from '../../environments/environment';
import {StudentGradeRemarkActionRoutingModule} from './student-grade-remark-action-routing.module';
import {ApplyComponent} from './apply/apply.component';
import {PreviewComponent} from './preview/preview.component';

import * as el from "./i18n/student-grade-remark-action.el.json"
import * as en from "./i18n/student-grade-remark-action.en.json"

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    AdvancedFormsModule,
    StudentGradeRemarkActionRoutingModule
  ],
  declarations: [ApplyComponent, PreviewComponent]
})
export class StudentGradeRemarkActionModule {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch( err => {
      console.error('An error occurred while loading StudentGradeRemarkActionModule.');
      console.error(err);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

}
